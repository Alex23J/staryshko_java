package com.company;

import com.company.Animals.Duck;
import com.company.Animals.Eagle;
import com.company.Animals.Penguin;

public class Main {

    public static void main(String[] args) {

        Duck duck = new Duck();
        duck.Feed("яблочко");
        duck.processFlying(0);

        Penguin penguin = new Penguin();
        penguin.Feed("рыбку");
        penguin.processSwimming(200);

        Eagle eagle = new Eagle();
        eagle.processRunning(100);
        eagle.processFlying(2000);

    }

}
