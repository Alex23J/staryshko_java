package com.company.Animals;

import com.company.Animals.Abstract.Bird;
import com.company.Animals.Abstract.FlyingBirds;
import com.company.Animals.Abstract.SwimmingAndFlyingBirds;
import com.company.Animals.Abstract.SwimmingBirds;

public class Activities {

    public void ActivityAirBird(FlyingBirds flyingBird, int flyingDistance, int runningDistance, String food)
    {
        flyingBird.processRunning(runningDistance);
        flyingBird.processFlying(flyingDistance);
        flyingBird.Feed(food);
    }

    public void ActivityWaterBird(SwimmingBirds swimmingBirds, int swimmingDistance, int runningDistance, String food)
    {
        swimmingBirds.processRunning(runningDistance);
        swimmingBirds.processSwimming(swimmingDistance);
        swimmingBirds.Feed(food);
    }

    public void ActivityWaterfowlBird(SwimmingAndFlyingBirds swimmingAndFlyingBirds, int swimmingDistance, int runningDistance, int flyingDistance, String food)
    {
        swimmingAndFlyingBirds.processRunning(runningDistance);
        swimmingAndFlyingBirds.processSwimming(swimmingDistance);
        swimmingAndFlyingBirds.processFlying(flyingDistance);
        swimmingAndFlyingBirds.Feed(food);
    }

    public void ActivityLegsBird(Bird bird, int runningDistance, String food)
    {
        bird.processRunning(runningDistance);
        bird.Feed(food);
    }

}
