package com.company.Animals;

import com.company.Animals.Abstract.Bird;

public class Kiwi extends Bird {

    @Override
    public void Feed(String food)
    {
        System.out.println(getClass().getSimpleName() + " сьел " + food);
    }

    @Override
    public void processRunning(int distance) {
        String className = getClass().getSimpleName();
        System.out.println(className + " пробежал " + distance + " метров ");
    }

}
