package com.company.Animals;

import com.company.Animals.Abstract.Bird;
import com.company.Animals.Possibilities.Fly;
import com.company.Animals.Possibilities.Swim;

public class Duck extends Bird implements Swim, Fly {

    @Override
    public void Feed(String food)
    {
        System.out.println(getClass().getSimpleName() + " сьел " + food);
    }

    @Override
    public void processFlying(int distance) {
        String className = getClass().getSimpleName();
        System.out.println(className + " пролетел " + distance + " метров ");
    }

    @Override
    public void processRunning(int distance) {
        String className = getClass().getSimpleName();
        System.out.println(className + " пробежал " + distance + " метров ");
    }

    @Override
    public void processSwimming(int distance) {
        String className = getClass().getSimpleName();
        System.out.println(className + " проплыл " + distance + " метров ");
    }
}
