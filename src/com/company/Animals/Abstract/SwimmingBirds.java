package com.company.Animals.Abstract;

import com.company.Animals.Possibilities.Swim;

public abstract class SwimmingBirds extends Bird implements Swim {

    @Override
    public void Feed(String food)
    {
        System.out.println(getClass().getSimpleName() + " сьел " + food);
    }

    @Override
    public void processRunning(int distance)
    {
        String className = getClass().getSimpleName();
        System.out.println(className + " пробежал: " + distance);
    }

    @Override
    public void processSwimming(int distance)
    {
        String className = getClass().getSimpleName();
        System.out.println(className + " проплыл: " + distance);
    }

}
