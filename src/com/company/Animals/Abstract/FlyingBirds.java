package com.company.Animals.Abstract;

import com.company.Animals.Possibilities.Fly;

public abstract class FlyingBirds extends Bird implements Fly{
    @Override
    public void Feed(String food)
    {
        System.out.println(getClass().getSimpleName() + " сьел " + food);
    }

    @Override
    public void processRunning(int distance)
    {
        String className = getClass().getSimpleName();
        System.out.println(className + " пробежал: " + distance);
    }

    @Override
    public void processFlying(int distance)
    {
        String className = getClass().getSimpleName();
        System.out.println(className + " пролетел: " + distance);
    }
}
