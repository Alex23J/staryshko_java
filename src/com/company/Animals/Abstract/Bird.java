package com.company.Animals.Abstract;

import com.company.Animals.Possibilities.Run;

public abstract class Bird extends Animals implements Run {

    @Override
    public void Feed(String food)
    {
        System.out.println(getClass().getSimpleName() + " сьел " + food);
    }

    @Override
    public void processRunning(int distance) {
        String className = getClass().getSimpleName();
        System.out.println(className + " бежит " + distance + " метров ");
    }

}
