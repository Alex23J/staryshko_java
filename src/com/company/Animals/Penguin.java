package com.company.Animals;

import com.company.Animals.Abstract.SwimmingBirds;

public class Penguin extends SwimmingBirds {

    @Override
    public void Feed(String food)
    {
        System.out.println(getClass().getSimpleName() + " сьел " + food);
    }

    @Override
    public void processRunning(int distance) {
        String className = getClass().getSimpleName();
        System.out.println(className + " пробежал " + distance + " метров ");
    }

    @Override
    public void processSwimming(int distance) {
        String className = getClass().getSimpleName();
        System.out.println(className + " проплыл " + distance + " метров ");
    }

}
