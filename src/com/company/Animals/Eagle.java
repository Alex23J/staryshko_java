package com.company.Animals;

import com.company.Animals.Abstract.FlyingBirds;

public class Eagle extends FlyingBirds {

    @Override
    public void Feed(String food)
    {
        System.out.println(getClass().getSimpleName() + " сьел " + food);
    }

    @Override
    public void processFlying(int distance) {
        String className = getClass().getSimpleName();
        System.out.println(className + " пролетел " + distance + " метров ");
    }

    @Override
    public void processRunning(int distance) {
        String className = getClass().getSimpleName();
        System.out.println(className + " пробежал " + distance + " метров ");
    }

}
