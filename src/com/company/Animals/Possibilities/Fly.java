package com.company.Animals.Possibilities;

public interface Fly {
    void processFlying(int distance);
}
