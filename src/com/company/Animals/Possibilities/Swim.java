package com.company.Animals.Possibilities;

public interface Swim {
    void processSwimming(int distance);
}
